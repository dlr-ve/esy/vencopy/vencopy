__maintainer__ = "Fabia Miorelli"
__license__ = "BSD-3-Clause"

import pytest
import pandas as pd

from typing import Any, Literal

from ....vencopy.core.dataparsers.dataparsers import DataParser
from ....vencopy.core.dataparsers.parseMiD import ParseMiD
from ....vencopy.core.dataparsers.parseKiD import ParseKiD
